$(function() {

// Фенсибокс
 $('.fancybox').fancybox({});




// BEGIN of script for certificate-slider
$('.certificate-slider').slick({
        slidesToShow: 5,
        slidesToScroll: 1,
        arrows: false,
        // autoplay: true,
        // autoplaySpeed: 4000,
        pauseOnHover: false,
        speed: 1000,
        dots: false,
        adaptiveHeight: true
    });
// END of script for certificate-slider





    $('.navbar-toggle').click(function() {
        $('.navbar-collapse').toggleClass('in');
        $('.navbar-nav').toggleClass('active');
    })

  $('.mobile-toggler').click(function() {
    $(this).toggleClass('active');
    $(this).parent('a').next(".dropdown-menu").stop(true,true).fadeToggle();
  })







// Фиксированнаня шапка при прокрутке
// $(window).scroll(function(){
//     var sticky = $('header'),
//         scroll = $(window).scrollTop();
//     if (scroll > 200) {
//         sticky.addClass('header-fixed');
//     } else {
//         sticky.removeClass('header-fixed');
//     };
// });









// BEGIN of script for back-to-top btn
    if ($('.back-to-top').length) {
        var scrollTrigger = 100, // px
        backToTop = function() {
            var scrollTop = $(window).scrollTop();
            if (scrollTop > scrollTrigger) {
                $('.back-to-top').addClass('show');
            } else {
                $('.back-to-top').removeClass('show');
            }
        };
        backToTop();
        $(window).on('scroll', function() {
            backToTop();
        });
        $('.back-to-top').on('click', function(e) {
            e.preventDefault();
            $('html,body').animate({
                scrollTop: 0
            }, 700);
        });
    }
// END of script for back-to-top btn



//Begin of GoogleMaps
  var myCenter=new google.maps.LatLng(55.768497, 37.572794);
  var marker;
  function initialize()
  {
    var mapProp = {
    center:myCenter,
    zoom:16,
    disableDefaultUI:true,
    zoomControl: true,
    scrollwheel: false,
    zoomControlOptions: {
        position: google.maps.ControlPosition.LEFT_CENTER
    },
    mapTypeId:google.maps.MapTypeId.ROADMAP
    };
      var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);
      var marker=new google.maps.Marker({
      position:myCenter,
      icon: "img/place.svg"
      // icon: "../img/marker.png" на  хосте может так рабоать     
      });
    marker.setMap(map);
    var content = document.createElement('div');
    content.innerHTML = '<strong class="maps-caption">Москва М. Грузинская 38</strong>';
    var infowindow = new google.maps.InfoWindow({
     content: content
    });
      google.maps.event.addListener(marker,'click',function() {
        infowindow.open(map, marker);
        map.setCenter(marker.getPosition());
      });
      }
      google.maps.event.addDomListener(window, 'load', initialize);
//End of GoogleMaps

})